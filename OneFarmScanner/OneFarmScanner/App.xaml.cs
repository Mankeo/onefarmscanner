using System;
using OneFarmScanner.Adapters;
using OneFarmScanner.Utils;
using OneFarmScanner.ViewModels;
using OneFarmScanner.Views.Phone;
using Prism;
using Prism.Ioc;
using Xamarin.Essentials.Implementation;
using Xamarin.Essentials.Interfaces;
using Xamarin.Forms;

namespace OneFarmScanner
{
    public partial class App
    {
        public App(IPlatformInitializer initializer)
            : base(initializer)
        {
        }

        protected override async void OnInitialized()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzczNTE2QDMxMzgyZTM0MmUzME5Pc0lBUFJWUVVDQjJNUTVnT2J5d0FHVC9oaml1WWZFOVZXNldLbjJUOXc9");
         
            InitializeComponent();

            var result = await NavigationService.NavigateAsync("NavigationPage/MainPage");

            if (!result.Success)
            {
                ErrorHelper.SetMainPageFromException(result.Exception);
            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IAuthAdapter, AuthAdapter>();
            containerRegistry.RegisterSingleton<ISowAdapter, SowAdapter>();
            containerRegistry.RegisterSingleton<IAppInfo, AppInfoImplementation>();
            
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
      
            containerRegistry.RegisterRegionServices();
            containerRegistry.RegisterForRegionNavigation<Settings, SettingsViewModel>();
            containerRegistry.RegisterForRegionNavigation<Scan, ScanViewModel>();
            containerRegistry.RegisterForRegionNavigation<ScanSowAction, ScanSowActionViewModel>();
            containerRegistry.RegisterForRegionNavigation<ScanBoarAction, ScanBoarActionViewModel>();
        }
    }
}
