﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneFarmScanner.Models
{
    public enum GenderType
    {
        Male = 0,
        Female = 1
    }

    public class PigModel
    {
        public Guid ExternalPigReference { get; set; }
        public string Designation { get; set; }
        public GenderType Gender { get; set; }
    }
}
