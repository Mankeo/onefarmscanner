﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OneFarmScanner.Models;
using RestSharp;

namespace OneFarmScanner.Adapters
{
    public interface ISowAdapter
    {
        IEnumerable<PigModel> GetSowsFromFilter(string filter);
    }

    public class SowAdapter : ISowAdapter
    {
        public IEnumerable<PigModel> GetSowsFromFilter(string filter)
        {
            var client = AuthAdapter.CreateClient($"api/v1/sow/{filter}");
            var restRequest = AuthAdapter.CreateDefaultRequest(Method.GET);

            var data = client.Execute<IEnumerable<PigModel>>(restRequest);

            //var restRequest = new RestRequest($"api/v1/sow/{filter}");
            //var client = AuthAdapter.GetClient(restRequest);

         //   var data = client.Get<IEnumerable<PigModel>>(restRequest);

            return data.Data;
        }

    }
}
