﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace OneFarmScanner.Adapters
{
    public interface IAuthAdapter
    {
        bool SignIn(string userName, string password);
        void SignOut();
    }

    public class AuthAdapter : IAuthAdapter
    {
        private static string _authToken = "";
        private static string _host = "";

        public AuthAdapter()
        {
#if DEBUGx
           _host = "https://localhost:5001";
#else
            _host = "https://onefarm.azurewebsites.net";
#endif
        }

        public bool SignIn(string userName, string password)
        {
            try
            {
                var client = new RestClient(_host);

                var authRequest = new RestRequest("api/v1/Authentication/signin");
                var signInData = new { UserName = userName, Password = password };
                authRequest.AddJsonBody(signInData, "application/json");
                var authResponse = client.Post(authRequest);

                if (authResponse.StatusCode == HttpStatusCode.OK)
                {
                    var authResponseBody = JObject.Parse(authResponse.Content);

                    _authToken = authResponseBody["token"].ToString();

                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public void SignOut()
        {
            //var authRequest = new RestRequest("/api/v1/Authentication/signout", Method.GET);
            //var client = GetClient(authRequest);
        }

        public static RestClient CreateClient(string resource)
        {
            var client = new RestClient($"{_host}/{resource}")
            {
                FollowRedirects = true
            };

            client.AddDefaultHeader("Authorization", $"Bearer {Token}");
           
            return client;
        }

        public static RestRequest CreateDefaultRequest(Method method)
        {
            var restRequest = new RestRequest(method);
            restRequest.AddHeader("cache-control", "no-cache");
            restRequest.AddHeader("Accept", "*/*");
            restRequest.AddHeader("Accept-Encoding", "gzip, deflate, br");
        //    restRequest.AddHeader("Cookie", "ARRAffinity=09e4dbcfb7d151ae26749cddf13422c292961bb7c3da19ef05434b6587763168; ARRAffinitySameSite=09e4dbcfb7d151ae26749cddf13422c292961bb7c3da19ef05434b6587763168; .AspNetCore.Identity.Application=CfDJ8LeS0kudAItCuHRzwlHEiZjbj9_C7CC_eaFxAi3QWcDxgWFcNVccfoL5mcr9594pHfRnpHT8XOJgJ5zkBXJZFbiKgMOiBCSXzgkY4lWUXWrWoaH3y_DkIecrsOJz9gfBrxN6Tia_7FwmjygiNTmPnuOCwneQWIgbIpVJlS_OZwqSYSM4O6Cm6oVvxN2UIOZhof-nHrW8Gv1aHn-vEGbq0aZJG_5VcbIe27Fn2VHcDqgN9sPS_6Xx0lRMODddap57QUYda544k_4gyja7jpCAmD2s8h3G0_bKTl_sKwGgTy_klcN2VBq1zt3c0LifnNGl0tVp5zQ3WNPFJx-Hi5DP_zxywwvXxwqxXG8Nf-qo6LinctL_pnfF3EM3-WfuRwKdsRQ236f5KGYDfUZ5oTUzD2VRnLCzn_zC1fSRW59tEWfLyUHeMFyx5hX6b-xLO3gtqv19Er_H_rGHUUV5qsWNF69rQrrcN6SJ0V_ND-D5i6XkaVSto8fpXzInvUt8qIQVtWAlvciprhZlTaDZpG_xKcAlhumWfPSl7h16SN9c3RfyVnUrBOwaUhe4Obn4MqtjGDxr018ADIyLQF2e0L-2hSoFH9jbe8o7vw-XmAkBwNRra8jKXqMaaFUy3t8l5YFNXJlvDS-77Vs7UPCxmOGynUXfjWmtHeZ5DfgRmLqDOouGKfcHODMe6wHrtgKrnEGS-1YVc-4I38Y_i46HAszTm2comWkUmNzZyVnNtLbath1XGSc1wU45C15eDoDTZ_hIpeK8ADBOR_H074uZmNqiDS3XF9yyohtDbjEFJd_A8zx08SiZiv7GZUhpungizYK_7sb2EvKS-x-l7ZYyw2tX3_Q");

            return restRequest;
        }

        public static string Token => _authToken;
        public static string Host => _host;

    }
}
