﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace OneFarmScanner.Views.Phone
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Settings : StackLayout
    {
        public Settings()
        {
            InitializeComponent();
        }
    }
}