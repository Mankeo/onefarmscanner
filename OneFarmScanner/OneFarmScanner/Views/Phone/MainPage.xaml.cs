﻿
using System;
using System.Diagnostics.CodeAnalysis;
using OneFarmScanner.Utils;
using Prism.Regions;
using Prism.Regions.Navigation;
using Xamarin.Forms;

namespace OneFarmScanner.Views.Phone
{
    [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
    public partial class MainPage
    {
        private readonly IRegionManager _regionManager;

        public MainPage(IRegionManager regionManager)
        {
            _regionManager = regionManager ?? throw new ArgumentNullException(nameof(regionManager));
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void Handle_Clicked(object sender, EventArgs e)
        {
            Syncfusion.XForms.Buttons.SfButton button = (sender as Syncfusion.XForms.Buttons.SfButton);

            if (button == IconScanButton)
            {
                IconScanButton.BackgroundColor = (Color)Application.Current.Resources["TabSelectedColor"];
                IconSettingsButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                IconActionsButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                IconQuitButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                _regionManager.RequestNavigate("ContentRegion", "Scan", RegionNavigationCallback);
            }
            else if (button == IconSettingsButton)
            {
                IconScanButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                IconSettingsButton.BackgroundColor = (Color)Application.Current.Resources["TabSelectedColor"];
                IconActionsButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                IconQuitButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                _regionManager.RequestNavigate("ContentRegion", "Settings", RegionNavigationCallback);
            }
            else if (button == IconActionsButton)
            {
                IconScanButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                IconSettingsButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                IconActionsButton.BackgroundColor = (Color)Application.Current.Resources["TabSelectedColor"];
                IconQuitButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
            }
            else if (button == IconQuitButton)
            {
                IconScanButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                IconSettingsButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                IconActionsButton.BackgroundColor = (Color)Application.Current.Resources["TabBackgroundColor"];
                IconQuitButton.BackgroundColor = (Color)Application.Current.Resources["TabSelectedColor"];
            }
        }

        private void RegionNavigationCallback(IRegionNavigationResult navigationResult)
        {
            if (navigationResult.Result.HasValue && navigationResult.Result.Value == false)
            {
                ErrorHelper.SetMainPageFromException(navigationResult.Error);
            }
        }
    }
}
