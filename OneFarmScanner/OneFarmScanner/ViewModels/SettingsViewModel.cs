﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Regions;
using Prism.Regions.Navigation;

namespace OneFarmScanner.ViewModels
{
    public class SettingsViewModel : BindableBase, IInitialize
    {
        private readonly IRegionManager _regionManager;

        public SettingsViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager ?? throw new ArgumentNullException(nameof(regionManager));
        }

        public void Initialize(INavigationParameters parameters)
        {
        }
    }
}
