﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OneFarmScanner.Adapters;
using OneFarmScanner.Utils;
using Prism.Regions;
using Prism.Regions.Navigation;

namespace OneFarmScanner.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private readonly IRegionManager _regionManager;
        private readonly IAuthAdapter _authAdapter;

        public MainPageViewModel(INavigationService navigationService, IRegionManager regionManager, IAuthAdapter authAdapter)
            : base(navigationService)
        {
            _regionManager = regionManager ?? throw new ArgumentNullException(nameof(regionManager));
            _authAdapter = authAdapter ?? throw new ArgumentNullException(nameof(authAdapter));
            Title = "OneFarm Scanner";
        }

        public override void Initialize(INavigationParameters parameters)
        {
            var result = _authAdapter.SignIn("Jompa", "yaa2Jonny");
            _regionManager.RequestNavigate("ContentRegion", "Scan", RegionNavigationCallback);
        }

        private void RegionNavigationCallback(IRegionNavigationResult navigationResult)
        {
            if (navigationResult.Result.HasValue && navigationResult.Result.Value == false)
            {
                ErrorHelper.SetMainPageFromException(navigationResult.Error);
            }
        }
    }
}
