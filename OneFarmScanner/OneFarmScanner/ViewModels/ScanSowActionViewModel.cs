﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Regions;
using Prism.Regions.Navigation;

namespace OneFarmScanner.ViewModels
{
    public class ScanSowActionViewModel : BindableBase, IInitialize
    {
        private readonly IRegionManager _regionManager;

        public ScanSowActionViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager ?? throw new ArgumentNullException(nameof(regionManager));
        }

        public void Initialize(INavigationParameters parameters)
        {
        }
    }
}
