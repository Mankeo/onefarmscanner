﻿using System;
using System.Collections.ObjectModel;
using OneFarmScanner.Adapters;
using OneFarmScanner.Models;
using OneFarmScanner.Utils;
using Prism.Navigation;
using Prism.Regions;
using Prism.Regions.Navigation;

namespace OneFarmScanner.ViewModels
{
    public class ScanViewModel : ViewModelBase
    {
        private readonly IRegionManager _regionManager;
        private readonly ISowAdapter _sowAdapter;

        public ScanViewModel(INavigationService navigationService, IRegionManager regionManager, ISowAdapter sowAdapter)
            : base(navigationService)
        {
            _regionManager = regionManager ?? throw new ArgumentNullException(nameof(regionManager));
            _sowAdapter = sowAdapter ?? throw new ArgumentNullException(nameof(sowAdapter));
            Title = "Scan";

            _sows = new ObservableCollection<PigModel>();
        }

        private ObservableCollection<PigModel> _sows;
        private PigModel _selectedItem;
        private string _sowFilterValueChanged;

        public ObservableCollection<PigModel> Sows
        {
            get => _sows;
            set => _sows = value;
        }

        public PigModel SelectedItem
        {
            get => _selectedItem;
            set
            {
                _selectedItem = value;

                if (_selectedItem != null)
                {
                    if (_selectedItem.Gender == GenderType.Female)
                    {
                        _regionManager.RequestNavigate("ContentRegion", "ScanSowAction", RegionNavigationCallback);
                    }
                    else if (_selectedItem.Gender == GenderType.Male)
                    {
                        _regionManager.RequestNavigate("ContentRegion", "ScanBoarAction", RegionNavigationCallback);
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("_selectedItem.Gender", $"Gender has an invalid value: {_selectedItem.Gender}");
                    }
                }
            }
        }

        public string SowFilterValueChanged
        {
            get => _sowFilterValueChanged;
            set
            {
                _sowFilterValueChanged = value;

                var sows = string.IsNullOrWhiteSpace(value) ? new ObservableCollection<PigModel>() : new ObservableCollection<PigModel>(_sowAdapter.GetSowsFromFilter(value));
                _sows.Clear();

                foreach (var sow in sows)
                {
                    _sows.Add(sow);
                }
            }
        }

        public override void Initialize(INavigationParameters parameters)
        {
        }

        private void RegionNavigationCallback(IRegionNavigationResult navigationResult)
        {
            if (navigationResult.Result.HasValue && navigationResult.Result.Value == false)
            {
                ErrorHelper.SetMainPageFromException(navigationResult.Error);
            }
        }
    }
}
