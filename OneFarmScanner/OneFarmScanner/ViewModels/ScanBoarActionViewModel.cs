﻿using System;
using System.Collections.Generic;
using System.Text;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Regions;
using Prism.Regions.Navigation;

namespace OneFarmScanner.ViewModels
{
    public class ScanBoarActionViewModel : BindableBase, IInitialize
    {
        private readonly IRegionManager _regionManager;

        public ScanBoarActionViewModel(IRegionManager regionManager)
        {
            _regionManager = regionManager ?? throw new ArgumentNullException(nameof(regionManager));
        }

        public void Initialize(INavigationParameters parameters)
        {
        }
    }
}
