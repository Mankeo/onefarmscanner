﻿using System;
using Xamarin.Forms;

namespace OneFarmScanner.Utils
{
    public class ErrorHelper
    {
        public static void SetMainPageFromException(Exception ex)
        {
            #if DEBUG
            var layout = new StackLayout
            {
                Padding = new Thickness(40)
            };
            layout.Children.Add(new Label
            {
                Text = ex?.GetType()?.Name ?? "Unknown Error encountered",
                FontAttributes = FontAttributes.Bold,
                HorizontalOptions = LayoutOptions.Center
            });

            layout.Children.Add(new ScrollView
            {
                Content = new Label
                {
                    Text = $"{ex}",
                    LineBreakMode = LineBreakMode.WordWrap
                }
            });

            Application.Current.MainPage = new ContentPage
            {
                Content = layout
            };
            #endif
        }
    }
}
